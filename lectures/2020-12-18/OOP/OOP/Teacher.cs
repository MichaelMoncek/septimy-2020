﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Teacher : Person
    {
        public Teacher(int x, int y, string name) : base(x, y, name)
        {
            base.job = "Ucitel";
            base.color = Color.Red;
        }
    }
}
