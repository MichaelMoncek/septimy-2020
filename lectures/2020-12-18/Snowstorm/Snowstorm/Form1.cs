﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Snowstorm
{
    public partial class Form1 : Form
    {
        List<Snowflake> snowflakes = new List<Snowflake>();
        //Snowflake[] snowflakesAr = new Snowflake[100];
        int v = 20;
        int size = 16;
        Random rnd = new Random();
        bool running = true;

        public Form1()
        {
            InitializeComponent();

            /*for (int i = 0; i < 100; i++)
            {
                snowflakesAr[i] = new Snowflake(rnd.Next(panel1.Width), -rnd.Next(panel1.Height), 16);
            }*/

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Font pismo = new Font("Arial", 16);
            g.DrawString("X", pismo, Brushes.Blue, 100, 80);
            /*g.TranslateTransform(116, 93);
            //g.RotateTransform(90);
            //g.TranslateTransform(-116, -93);*/

            //nejjednodusi vykresleni
            foreach (Snowflake sf in snowflakes)
            //foreach (Snowflake sf in snowflakesAr)
            {
                sf.Draw(g);
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Snowflake snowflake = new Snowflake(rnd.Next(panel1.Width), 0, 16);
            if (running)
                snowflakes.Add(snowflake);
            
            //
            if (snowflakes.Count > 10)
                snowflakes.RemoveAt(0);


            foreach (Snowflake sf in snowflakes)
            //foreach (Snowflake sf in snowflakesAr)
            {
                sf.Move(v, size);
                //sf.Move(v, this.Height);
            }

            Refresh();
        }

        private void snowButton_Click(object sender, EventArgs e)
        {
            running = !running;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            size = trackBar1.Value;
        }
    }
}
