B
Na vypracování teoretické části máte 30 minut.
Některé odpovědi se mohou lišit v jiných programovacích jazycích, ptám se na C# a "svět" .NET Frameworku.

15b

a) Co je to OOP? Vysvětlete, z čeho se skládá třída a co znamená zapouzdření. Jak se liší private, protected a public. (5b)

b) Co je to v OOP Interface a jak se liší od normální třídy? Jak v C# zajistíme dědičnost (třida vs interface)? (5b)

c) Vysvětlete, jak funguje Váš řadící algoritmus. (5b)







