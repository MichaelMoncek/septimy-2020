﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gui2
{
    public partial class Form1 : Form
    {
        int[] arr;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonRewrite_Click(object sender, EventArgs e)
        {
            label1.Text = textBox1.Text;
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            //textBoxGenerated.Clear();

            arr = new int[20];
            Random rnd = new Random();
            for (int i = 0; i < 20; i++)
            {
                arr[i] = rnd.Next(0, 100);

                //textBoxGenerated.Text += arr[i] + " ";
            }

            textBoxGenerated.Text = String.Join(" ", arr);        
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            InsertionSort(arr);
            textBoxSort.Text = String.Join(" ", arr);
        }


        private void InsertionSort(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                int j = i + 1;
                int temp = array[j];
                while(j > 0 && temp < array[j - 1])
                {
                    array[j] = array[j - 1];
                    j--;
                }
                array[j] = temp;
            }
        }
    }
}
