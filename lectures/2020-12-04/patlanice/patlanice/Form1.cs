﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace patlanice
{
    public partial class Form1 : Form
    {
        int x, y = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics kp = e.Graphics;

            //kp.DrawRectangle(Pens.Red, 0, 0, 50, 50);
            kp.FillRectangle(Brushes.Red, x, y, 50, 50);
        }

        private void Form1_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
                timer1.Enabled = false;
            else
                timer1.Enabled = true;

            //timer1.Enabled = !timer1.Enabled;
        }

        // Zaprvy se musi nastavit keyPreview aby form zpracovaval udalosti pred tlacitkem
        // zadruhy si clovek musi dat pozor na control keys, to pak musi prepsat tuhle metodu
        // Nebo dat keyUp a to pak funguje i na control key
        /*protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Down)
                y += 10;
            return base.ProcessCmdKey(ref msg, keyData);
        }*/

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            //tahle udalost se vyvola pouze na ovladacich prvcich na kterych je nastaven focus
            //

            /*switch(e.KeyCode)
            {
                case Keys.S:
                    y += 10;
                    break;
            }*/


            if (e.KeyCode == Keys.S)
            {
                y += 10;
                if (e.KeyCode == Keys.D)
                    x += 10;
            }
            if(e.KeyCode == Keys.W)
                y -= 10;
            if(e.KeyCode == Keys.D)
                x += 10;
            if(e.KeyCode == Keys.A)
                x -= 10;

            //e.Handled = true;
            Refresh();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //MessageBox.Show(e.KeyChar.ToString());

            if (e.KeyChar == 'h' && e.KeyChar == 'u') { 
                y += 10;
                y -= 10;

                MessageBox.Show("Ahoj");
            }
            
            if (e.KeyChar == 'h') {
                y += 10;

            }

            Refresh();
        }

        private void button2DArray_Click(object sender, EventArgs e)
        {
            int[,] array2d = new int[5, 5];

            int[][] array2d2 = new int[5][];
            for (int i = 0; i < 5; i++)
            {
                array2d2[i] = new int[i*10];
            }

                  //S, R
            array2d[0, 0] = 1; 
            array2d[4, 0] = 2; 

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < array2d.GetLength(0); i++)
            {
                for (int j = 0; j < array2d.GetLength(1); j++)
                {
                    sb.Append(array2d[j, i]);
                    sb.Append(" ");
                }
                sb.AppendLine();
            }

            richTextBox1.Text = sb.ToString();
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            x += 10;
            Refresh();
        }
    }
}
