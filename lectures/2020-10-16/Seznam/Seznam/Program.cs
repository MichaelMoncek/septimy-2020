﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Seznam
{
    class Program
    {
        static void Main(string[] args)
        {
            //Seznamy
            List<double> numbers = new List<double>();
            numbers.Add(7.32);    // vlozi prvek na konec
            numbers.Insert(0, 3); // vloží prvek na danou pozici (cislovani od nuly!)

            numbers.Add(2);
            numbers.Add(-7);
            numbers.Add(20145);
            numbers.Add(0);
            numbers.Add(777);

            numbers.Remove(7.322); // odebere prvek, ktery mu rekneme
            numbers.RemoveAt(1);  // odebere prvek na dane pozici (cislovani od nuly!)

            //numbers.Clear();       // smaze vsechny prvky
            double[] poleCisel = numbers.ToArray(); // prevede List<> na pole
            Console.WriteLine(poleCisel[0]);
            printArrayD(poleCisel);
            Console.WriteLine();

            List<double> druhyList = poleCisel.ToList();    // prevede pole na List<>






            //Vytvorte pole o 30 prvcich a naplnte ho nahodnymi cisly
            int[] pole = new int[30];
            Random rnd = new Random();
            for (int i = 0; i < 30; i++)
            {
                pole[i] = rnd.Next(1, 100);
            }

            //Vytverte dva seznamy pro prvocisla a slozena cisla
            List<int> prvocisla = new List<int>();
            List<int> slozenaCisla = new List<int>();

            foreach (int cislo in pole)
            {
                if (isProvcislo(cislo))
                {
                    prvocisla.Add(cislo);
                }
                else
                {
                    slozenaCisla.Add(cislo);
                }
            }


            printArray(pole);
            printArray(prvocisla.ToArray());
            printArray(slozenaCisla.ToArray());



            //genericita
            Trida<int> instance = new Trida<int>(10);
            Console.WriteLine(instance.getPromenna());

            Console.ReadKey();
        }

        static bool isProvcislo(int x)
        {
            if (x <= 1)
                return false;

            for (int i = 2; i < Math.Sqrt(x); i++)
            {
                if (x % i == 0)
                {
                    return false;
                }
            }

            return true;
        }

        static void printArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }

        static void printArrayD(double[] array)
        {
            foreach (double item in array)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();
        }
    }

    public class Trida<T>
    {
        private T promenna;

        public Trida(T promenna)
        {
            this.promenna = promenna;
        }

        public T getPromenna()
        {
            return promenna;
        }
    }
}
